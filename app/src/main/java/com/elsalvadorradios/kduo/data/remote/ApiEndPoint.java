package com.elsalvadorradios.kduo.data.remote;/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import com.elsalvadorradios.kduo.BuildConfig;

/**
 * Created by amitshekhar on 07/07/17.
 */

public final class ApiEndPoint {




    public static final String ENDPOINT_LOGOUT = BuildConfig.BASE_URL ;


    public static final String ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL + "login";
    public static final String ENDPOINT_HISTORY = BuildConfig.BASE_URL;
    public static final String ENDPOINT_CHANNELPLAY = BuildConfig.BASE_URL+"channels/";
    public static final String ENDPOINT_SERVER_STATION = BuildConfig.BASE_URL
            + "/station";
    public static final String ENDPOINT_SERVER_CHANNELS = BuildConfig.BASE_URL + "user/channels" ;
    public static final String ENDPOINT_REGISTER = BuildConfig.BASE_URL + "register";


    private ApiEndPoint() {
        // This class is not publicly instantiable
    }
}
