package com.elsalvadorradios.kduo.ui.radio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.elsalvadorradios.kduo.R;
import com.elsalvadorradios.kduo.data.model.api.AppStation;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

public class RadioListAdapter extends ArrayAdapter<AppStation> {
    List<AppStation> appStations=new ArrayList<>();
    Context context;


    public RadioListAdapter(Context context, List<AppStation> appStations) {
        super(context, -1, appStations);
        this.context = context;
        this.appStations = appStations;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        AppStation currentAppStation=appStations.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_radio_list, parent, false);


        TextView textView = (TextView) rowView.findViewById(R.id.list_item_textview);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.list_item_station_icon);
        ImageView favorite = (ImageView) rowView.findViewById(R.id.list_item_favourite_button);
        textView.setText(currentAppStation.getRadio_title());
//        Picasso.get()
//                .load(currentAppStation.getImage_url())
//                .placeholder(R.drawable.progress_animaton)
//                .error(R.drawable.progress_animaton)
//                .into(imageView);



        return rowView;
    }
}
