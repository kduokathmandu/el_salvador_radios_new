package com.elsalvadorradios.kduo.ui.splash;/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

import com.elsalvadorradios.kduo.data.DataManager;
import com.elsalvadorradios.kduo.ui.base.BaseViewModel;
import com.elsalvadorradios.kduo.utils.rx.SchedulerProvider;

/**
 * Created by amitshekhar on 08/07/17.
 */

public class SplashViewModel extends BaseViewModel<SplashNavigator> {

    public SplashViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }


    public void decideNextActivity() {
        System.out.println(getDataManager().getCurrentUserLoggedInMode());
//        if (getDataManager().getCurrentUserLoggedInMode() == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
            getNavigator().openMainActivity();
//        } else {
//            Log.d("Checkingusersswotd",getDataManager().getCurrentUserName()+"   "+getDataManager().getCurrentPassword());
//            getCompositeDisposable().add(getDataManager()
//                    .doServerLoginApiCall(new LoginRequest.ServerLoginRequest(getDataManager().getCurrentUserEmail()
//                            ,getDataManager().getCurrentPassword(),getDataManager().getType()))
//                    .doOnSuccess(response -> getDataManager()
//                            .updateUserInfo(
//                                    getDataManager().getCurrentUserName(),
//                                    getDataManager().getCurrentPassword(),
//                                    getDataManager().getType(),
//                                    DataManager.LoggedInMode.LOGGED_IN_MODE_SERVER
//                            ))
//                    .subscribeOn(getSchedulerProvider().io())
//                    .observeOn(getSchedulerProvider().ui())
//                    .subscribe(new Consumer<LoginResponse>() {
//                        @Override
//                        public void accept(LoginResponse loginResponse) throws Exception {
//                            setIsLoading(false);
//                            Log.d("CheckingHere", "yes");
//                            getDataManager().getApiHeader().getProtectedApiHeader().setAccessToken("Bearer " + loginResponse.getToken());
//                            getNavigator().openMainActivity();
//                        }
//                    }, new Consumer<Throwable>() {
//                        @Override
//                        public void accept(Throwable throwable) throws Exception {
//                            getNavigator().handleError(throwable);
//                        }
//                    }));
//
//        }
    }
}
