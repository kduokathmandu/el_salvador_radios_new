package com.elsalvadorradios.kduo.ui.splash;/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.elsalvadorradios.kduo.BR;
import com.elsalvadorradios.kduo.R;
import com.elsalvadorradios.kduo.databinding.ActivitySplashBinding;
import com.elsalvadorradios.kduo.ui.base.BaseActivity;
import com.elsalvadorradios.kduo.ui.main.MainActivity;

import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by amitshekhar on 08/07/17.
 */

public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel> implements SplashNavigator {

    @Inject
    SplashViewModel mSplashViewModel;

    public static Intent newIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        return mSplashViewModel;
    }



    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.newIntent(SplashActivity.this);

        startActivity(intent);
        finish();
    }

    @Override
    public void handleError(Throwable throwable) {
        HttpException error = (HttpException)throwable;

        try {
            String errorBody = error.response().errorBody().string();
            showSnackBar(errorBody);
            Log.d("tagchecking", errorBody);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("code", ((HttpException) throwable).code() + "");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSplashViewModel.setNavigator(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSplashViewModel.decideNextActivity();
            }
        },3000);
//        openLoginActivity();
    }
}
