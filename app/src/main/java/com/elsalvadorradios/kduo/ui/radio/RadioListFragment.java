package com.elsalvadorradios.kduo.ui.radio;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.elsalvadorradios.kduo.BuildConfig;
import com.elsalvadorradios.kduo.R;
import com.elsalvadorradios.kduo.data.model.api.AppStation;
import com.elsalvadorradios.kduo.ui.main.MainActivity;
import com.elsalvadorradios.kduo.ui.radio.player.RadioManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import bolts.Task;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RadioListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RadioListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RadioListFragment extends Fragment implements View.OnClickListener,ListView.OnItemClickListener {

    private String streamURL;


    private OnFragmentInteractionListener mListener;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    FirebaseDatabase database;
    DatabaseReference myRef;
    RadioManager radioManager;

    ImageButton trigger;
    ListView listView;
    TextView textView;
    View subPlayer;



    private List<AppStation> radioStations = new ArrayList<>();
    private AppStation appStation;
    ProgressDialog loadingDialog;
    // Remote Config keys
    private static final String LOADING_PHRASE_CONFIG_KEY = "loading_phrase";
    private static final String WELCOME_MESSAGE_KEY = "welcome_message";
    private static final String WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps";



    public RadioListFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RadioListFragment newInstance() {
        RadioListFragment fragment = new RadioListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         database = FirebaseDatabase.getInstance();
         myRef = database.getReference("stations/el_salvador_radios");
        radioManager = RadioManager.with(getActivity());


        OneSignal.startInit(getActivity())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        OneSignal.setEmail("binisha.shrestha@kduoapps.com");

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();


        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mRootView =  inflater.inflate(R.layout.fragment_radio_list, container, false);


        bindFragment(mRootView);

        getData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(), "sdvasdcasdcsdcsdc", Toast.LENGTH_SHORT).show();
            }
        });
        return mRootView;

    }
    private void bindFragment(View rootView) {

        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setMessage("Loading...");
        loadingDialog.setCancelable(false);
        loadingDialog.show();


        trigger=(ImageButton)rootView.findViewById(R.id.playTrigger);
        listView=(ListView)rootView.findViewById(R.id.listview);
        textView=(TextView)rootView.findViewById(R.id.name);
        subPlayer=(View)rootView.findViewById(R.id.sub_player);

    }

    private void getData() {
        if (isNetworkConnected()) {
           myRef .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        appStation = dataSnapshot1.getValue(AppStation.class);

                        if (appStation != null) {
                            radioStations.add(new AppStation(appStation.image_url, appStation.radio_title, appStation.stream_url));
                        }
                    }
                    listView.setAdapter(new RadioListAdapter(getActivity(), radioStations));
                    loadingDialog.dismiss();

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.i("stations", databaseError.toString());
                    loadingDialog.dismiss();

                }
            });
        } else {
            showDialog();
        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void showDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle("No Internet Connection.")
                .setMessage("Try Again??")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        getData();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        getActivity().finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.playTrigger:
                if (TextUtils.isEmpty(streamURL)) return;

                radioManager.playOrPause(streamURL);
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Toast.makeText(getActivity(), "asdcasdcawdcsadc", Toast.LENGTH_SHORT).show();
//            AppStation appStation = (AppStation) adapterView.getItemAtPosition(i);
//            if (appStation == null) {
//
//                return;
//
//            }
//
//            textView.setText(appStation.getRadio_title());
//
//            subPlayer.setVisibility(View.VISIBLE);
//
//            streamURL = appStation.getStream_url();
//
//            radioManager.playOrPause(streamURL);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
