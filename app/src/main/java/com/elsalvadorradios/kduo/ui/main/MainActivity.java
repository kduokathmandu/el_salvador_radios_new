package com.elsalvadorradios.kduo.ui.main;/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.elsalvadorradios.kduo.R;
import com.elsalvadorradios.kduo.ui.radio.RadioListFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.elsalvadorradios.kduo.BR;
import com.elsalvadorradios.kduo.BuildConfig;
import com.elsalvadorradios.kduo.databinding.ActivityMainBinding;
import com.elsalvadorradios.kduo.databinding.NavHeaderMainBinding;
import com.elsalvadorradios.kduo.ui.base.BaseActivity;
import com.onesignal.OneSignal;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.inject.Inject;

import bolts.Task;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator {


    private static final String TAG = "TAG";
    private DrawerLayout mDrawer;
    private MainViewModel mMainViewModel;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;
    private String experiment1_variant;


    AdView mAdView;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    @Inject
    MainPagerAdapter mPagerAdapter;

    private ActivityMainBinding mActivityMainBinding;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    public MainViewModel getViewModel() {
        mMainViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MainViewModel.class);
        return mMainViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        HttpException error = (HttpException)throwable;

        try {
            String errorBody = error.response().errorBody().string();
            showSnackBar(errorBody);
            Log.d("tagchecking", errorBody);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("code", ((HttpException) throwable).code() + "");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMainBinding = getViewDataBinding();

        String version = getString(R.string.version) + " " + BuildConfig.VERSION_NAME;

        mMainViewModel.setNavigator(this);
        setUp();
        setupRemoteConfig();


        mMainViewModel.updateAppVersion(version);
        mMainViewModel.onNavMenuCreated();
//        mMainViewModel.getChannels();

        MobileAds.initialize(this, getString(R.string.google_ads_app_id));
        changeToolbarColor(true);

        loadFragment(new RadioListFragment());

        mAdView = mActivityMainBinding.adView;
        mAdView.bringToFront();
        AdRequest request = new AdRequest.Builder()
                .addTestDevice("AD23356FE52420C1498BB2818D0CA5CD")
                .build();

        mAdView.loadAd(request);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });


        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void loadFragment(RadioListFragment radioListFragment) {
        Fragment fragment = new RadioListFragment();
        Bundle b = new Bundle();

        getSupportFragmentManager().beginTransaction().replace(R.id.flFrame, fragment).commit();
    }

    private void setUp() {
        mDrawer = mActivityMainBinding.drawerView;
        mToolbar = mActivityMainBinding.toolbar;
        mNavigationView = mActivityMainBinding.navigationView;

        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawer,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.nav);
        mDrawer.addDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
        setupNavMenu();

        mPagerAdapter.setCount(2);
        mActivityMainBinding.mainViewPager.setAdapter(mPagerAdapter);

        mActivityMainBinding.tabLayout.addTab(mActivityMainBinding.tabLayout.newTab().setIcon(R.drawable.ic_radio_black_24dp));
        mActivityMainBinding.tabLayout.addTab(mActivityMainBinding.tabLayout.newTab().setIcon(R.drawable.ic_star));

        mActivityMainBinding.mainViewPager.setOffscreenPageLimit(mActivityMainBinding.tabLayout.getTabCount());

        mActivityMainBinding.mainViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mActivityMainBinding.tabLayout));

        mActivityMainBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mActivityMainBinding.mainViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setupRemoteConfig() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings
                .Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

                        Log.d("IID_TOKEN", refreshedToken);


        fetchData();
        setEventListener();

    }

    private void fetchData() {
        long cacheExpiration = 3600; // 1 hour in seconds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                    experiment1_variant = mFirebaseRemoteConfig.getString("experiment1");
                    mFirebaseAnalytics.setUserProperty("MyExperiment", experiment1_variant);

                    if ("Top".equals(experiment1_variant)) {
                        mActivityMainBinding.bubbleTop.setVisibility(View.VISIBLE);
                    } else if ("Bottom".equals(experiment1_variant)) {
                        mActivityMainBinding.bubbleBottom.setVisibility(View.VISIBLE);
                    }
                }
            }


        });
    }


    private void setEventListener() {
        mActivityMainBinding.bubbleTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logEventToFirebase();
            }
        });
        mActivityMainBinding.bubbleBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logEventToFirebase();
            }
        });
    }

    private void logEventToFirebase() {
        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.LOCATION, experiment1_variant);
        mFirebaseAnalytics.logEvent("BubbleClicked", params);
        mActivityMainBinding.bubbleTop.setVisibility(View.GONE);
        mActivityMainBinding.bubbleBottom.setVisibility(View.GONE);
    }
    private void setupNavMenu() {
        NavHeaderMainBinding navHeaderMainBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.nav_header_main, mActivityMainBinding.navigationView, false);
        mActivityMainBinding.navigationView.addHeaderView(navHeaderMainBinding.getRoot());
        navHeaderMainBinding.setViewModel(mMainViewModel);

        mNavigationView.setNavigationItemSelectedListener(
                item -> {
                    mDrawer.closeDrawer(GravityCompat.START);
                    switch (item.getItemId()) {
                        case R.id.navItemSleepTimer:

                            return true;
                        case R.id.navItemSettings:
//
                            return true;
                        case R.id.navItemAbout:
                            return true;
                        default:
                            return false;
                    }
                });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


git
    public void changeToolbarColor(boolean flag) {
        if (flag) {
            mToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.kduo_black));
        } else {
            mToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
            getSupportActionBar().setTitle("");
        }
    }


}
